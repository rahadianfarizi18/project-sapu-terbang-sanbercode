<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cog\Contracts\Love\Reactable\Models\Reactable as ReactableInterface;
use Cog\Laravel\Love\Reactable\Models\Traits\Reactable;

class Pertanyaan extends Model implements ReactableInterface
{
    use Reactable;
    protected $table = 'pertanyaan';
    protected $guarded = [];

    public function user(){
        return $this->belongsTo('App\User','profil_id');
    }

    public function jawabanTepat(){
        return $this->hasOne('App\Jawaban','jawaban_tepat_id');
    }

    public function tag(){
        return $this->belongsToMany('App\Tag');
    }

    public function jawaban(){
        return $this->hasMany('App\Jawaban');
    }

    public function komentar_pertanyaan(){
        return $this->hasMany('App\komentarPertanyaan','pertanyaan_id');
    }
}

<?php

namespace App;

use Cog\Contracts\Love\Reactable\Models\Reactable as ReactableInterface;
use Cog\Laravel\Love\Reactable\Models\Traits\Reactable;
use Illuminate\Database\Eloquent\Model;

class Jawaban extends Model implements ReactableInterface
{
    use Reactable;
    protected $table = 'jawaban';
    protected $guarded = [];

    public function user(){
        return $this->belongsTo('App\User','profil_id');
    }

    public function pertanyaan_tepat(){
        return $this->hasOne('App\Pertanyaan','jawaban_tepat_id');
    }

    public function pertanyaan(){
        return $this->belongsTo('App\Pertanyaan','pertanyaan_id');
    }

    public function komentar_jawaban(){
        return $this->hasMany('App\komentarJawaban','jawaban_id');
    }

}

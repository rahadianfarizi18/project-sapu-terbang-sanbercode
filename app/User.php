<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Cog\Contracts\Love\Reacterable\Models\Reacterable as ReacterableInterface;
use Cog\Laravel\Love\Reacterable\Models\Traits\Reacterable;

class User extends Authenticatable implements ReacterableInterface
{
    use Reacterable;

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','username'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function pertanyaan(){
        return $this->hasMany('App\Pertanyaan', 'profil_id');
    }

    public function jawaban(){
        return $this->hasMany('App\Jawaban', 'profil_id');
    }

    public function komentar_jawaban(){
        return $this->hasMany('App\komentarJawaban','profil_id');
    }

    public function komentar_pertanyaan(){
        return $this->hasMany('App\komentarPertanyaan','profil_id');
    }


}

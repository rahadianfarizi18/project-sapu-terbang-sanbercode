<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use Auth;
Use Alert;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function logout()
    {
        Alert::success('Berhasil', 'Anda berhasil logout.');
        Auth::logout();
        return app('App\Http\Controllers\PertanyaanController')->index();
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jawaban;
use App\Pertanyaan;
use App\komentarJawaban;
use Auth;
use Alert;

class komentarJawabanController extends Controller
{
    public function __construct(){
        $this->middleware('auth') -> except(['index']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($idPertanyaan, $idJawaban)
    {
        $pertanyaan = Pertanyaan::find($idPertanyaan);
        $jawaban = Jawaban::find($idJawaban);
        return view('komentar_jawaban.create', compact('jawaban','pertanyaan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        date_default_timezone_set('Asia/Jakarta');
        $request->validate([
            'isi_komentar_jawaban' => 'required'
        ]);

        $user = Auth::user();

        $pertanyaan = $request['id_pertanyaan'];

        $komentar_jawaban = $user->komentar_jawaban() -> create([
            'isi' => $request['isi_komentar_jawaban'],
            'tanggal_dibuat' => date('y/m/d'),
            'jawaban_id' => $request['id_jawaban']
        ]);

        $jawaban = $request['id_jawaban'];

        Alert::success('Berhasil', 'Komentar berhasil disimpan!');

        return redirect() -> route('pertanyaan.show',["pertanyaan"=>$pertanyaan]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $komentar_jawaban = komentarJawaban::with('user')->find($id);
        return view('komentarJawaban.edit',compact('$komentar_jawaban'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        date_default_timezone_set('Asia/Jakarta');
        $request->validate([
            'isi_komentar_jawaban' => 'required'
        ]);

        $user = Auth::user();
        $komentar_jawaban = $user->komentar_jawaban() -> update([
            'isi' => $request['isi_komentar_jawaban']
        ]);

        $jawaban = $request['id_jawaban'];
        Alert::success('Berhasil', 'Komentar berhasil diubah!');
        return redirect()->route('pertanyaan.show',["pertanyaan"=>$pertanyaan]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {

        komentarJawaban::destroy($id);
        $pertanyaan = $request['id_pertanyaan'];
        Alert::info('Berhasil', 'Komentar berhasil dihapus!');
        return redirect()->route('pertanyaan.show',["pertanyaan"=>$pertanyaan]);
    }

    public function like($id, Request $request)
    {
        $user = Auth::user();
        $reacterFacade = $user->viaLoveReacter();

        $komentar_jawaban = komentarJawaban::find($id);
        $reacterFacade->reactTo($komentar_jawaban, 'Like');
        $pertanyaan = $komentar_jawaban -> jawaban -> pertanyaan -> id;

        return redirect()->route('pertanyaan.show',["pertanyaan"=>$pertanyaan]);
    }

    public function dislike($id, Request $request)
    {
        $user = Auth::user();
        $reacterFacade = $user->viaLoveReacter();

        $komentar_jawaban = komentarJawaban::find($id);
        $reacterFacade->reactTo($komentar_jawaban, 'Dislike');
        $pertanyaan = $komentar_jawaban -> jawaban -> pertanyaan -> id;

        return redirect()->route('pertanyaan.show',["pertanyaan"=>$pertanyaan]);
    }
}

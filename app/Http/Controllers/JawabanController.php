<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pertanyaan;
use App\Jawaban;
use Auth;
use Alert;

class JawabanController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth') -> except(['index']) ;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $data_jawaban = Jawaban::all();
        // return view('jawaban.index', compact('data_jawaban'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $pertanyaan = Pertanyaan::find($id);
        return view('jawaban.create',compact('pertanyaan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        $request->validate([
            'isi_jawaban' => 'required'
        ]);

        $user = Auth::user();

        $jawaban = $user->jawaban() -> create([
            'isi' => $request['isi_jawaban'],
            'tanggal_dibuat' => date('y/m/d'),
            'pertanyaan_id' => $request['id_pertanyaan']

        ]);

        $pertanyaan = $request['id_pertanyaan'];

        Alert::success('Berhasil', 'Jawaban berhasil disimpan!');

        return redirect()->route('pertanyaan.show',["pertanyaan"=>$pertanyaan]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $jawaban = Jawaban::with('user')->find($id);
        return view('jawaban.edit',compact('jawaban'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        date_default_timezone_set('Asia/Jakarta');
        $request->validate([
            'isi_jawaban' => 'required'
        ]);

        $user = Auth::user();

        $jawaban = $user->jawaban() -> update([
            'isi' => $request['isi_jawaban'],
            'tanggal_diperbarui' => date('y/m/d'),

        ]);

        $pertanyaan = $request['id_pertanyaan'];

        Alert::success('Berhasil', 'Jawaban berhasil diubah!');

        return redirect()->route('pertanyaan.show',["pertanyaan"=>$pertanyaan]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        Jawaban::destroy($id);
        Alert::warning('Berhasil', 'Jawaban berhasil dihapus!');
        $pertanyaan = $request['id_pertanyaan'];
        return redirect()->route('pertanyaan.show',["pertanyaan"=>$pertanyaan]);
    }

    public function like($id, Request $request)
    {
        $user = Auth::user();
        $reacterFacade = $user->viaLoveReacter();

        $jawaban = Jawaban::find($id);
        $reacterFacade->reactTo($jawaban, 'Like');
        $pertanyaan = $jawaban -> pertanyaan -> id;

        return redirect()->route('pertanyaan.show',["pertanyaan"=>$pertanyaan]);
    }

    public function dislike($id, Request $request)
    {
        $user = Auth::user();
        $reacterFacade = $user->viaLoveReacter();

        $jawaban = Jawaban::find($id);
        $reacterFacade->reactTo($jawaban, 'Dislike');
        $pertanyaan = $jawaban -> pertanyaan -> id;

        return redirect()->route('pertanyaan.show',["pertanyaan"=>$pertanyaan]);
    }
}

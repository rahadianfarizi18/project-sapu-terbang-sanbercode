<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Pertanyaan;
use App\Jawaban;
use App\Profil;
use App\komentarJawaban;
Use Auth;
use App\Tag;
use App\Exports\PertanyaanExport;
use Maatwebsite\Excel\Facades\Excel;
use Alert;
Use ReactionType;

class PertanyaanController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth') -> except(['index']) ;
    }

    public function index(){
        $data_pertanyaan = Pertanyaan::all();
        // $reactantFacade = $data_pertanyaan->viaLoveReactant();
        return view('pertanyaan.index', compact('data_pertanyaan'));
    }

    public function create(){
        return view('pertanyaan.create');
    }

    public function store(Request $request){
        date_default_timezone_set('Asia/Jakarta');
        $request->validate([
            'judul_pertanyaan' => 'required|max:255',
            'isi_pertanyaan' => 'required'
        ]);

        $tags_arr = explode(',',$request['tag']);

        $tag_ids = [];
        foreach($tags_arr as $tag_name){
            $tag = Tag::firstOrCreate(['tag' => $tag_name]);
            $tag_ids[] = $tag->id;
        }

        $user = Auth::user();

        $pertanyaan = $user->pertanyaan() -> create([
            'judul' => $request['judul_pertanyaan'],
            'isi' => $request['isi_pertanyaan'],
            'tanggal_dibuat' => date('y/m/d')

        ]);

        $pertanyaan->tag()->sync($tag_ids);


        Alert::success('Berhasil', 'Pertanyaan berhasil disimpan!');

        return redirect('/pertanyaan')->with('success', 'Pertanyaan berhasil disimpan!');
    }

    public function update($id, Request $request){
        date_default_timezone_set('Asia/Jakarta');
        $request->validate([
            'judul_pertanyaan' => 'required|max:255',
            'isi_pertanyaan' => 'required'
        ]);

        $pertanyaan = Pertanyaan::find($id) -> update ([
            'judul' => $request['judul_pertanyaan'],
            'isi' => $request['isi_pertanyaan'],
            'tanggal_diperbarui' => date('y/m/d'),
        ]);
        return redirect('/pertanyaan')->with('success', 'Data pertanyaan berhasil diubah!')  ;
    }

    public function show($id){
        $pertanyaan = Pertanyaan::find($id);
        $data_jawaban = Jawaban::where('pertanyaan_id',$id)->get();
        $data_komentar_jawaban = komentarJawaban::with('jawaban')->get();
        $reactantFacade = $pertanyaan->viaLoveReactant();
        $data_komentar_jawaban = komentarJawaban::all();
        // $reaction_like_counter = $reactantFacade->getReactionCounterOfType('Like');
        return view('pertanyaan.show',compact('pertanyaan','data_jawaban','reactantFacade','data_komentar_jawaban'));
    }

    public function edit($id){
        $pertanyaan = Pertanyaan::with('user')->find($id);
        $tags_arr = [];
        foreach($pertanyaan -> tag as $tag){
            $tags_arr[] = $tag->tag;
        }
        return view('pertanyaan.edit',compact('pertanyaan','tags_arr'));
    }

    public function destroy($id){
        Pertanyaan::destroy($id);
        return redirect('/pertanyaan')->with('success', 'Data berhasil dihapus!')  ;
    }

    public function export()
    {
        return Excel::download(new PertanyaanExport, 'pertanyaan.xlsx');
    }

    public function like($id)
    {
        $user = Auth::user();
        $reacterFacade = $user->viaLoveReacter();

        $pertanyaan = Pertanyaan::find($id);
        $reacterFacade->reactTo($pertanyaan, 'Like');

        return redirect()->route('pertanyaan.show',["pertanyaan"=>$pertanyaan]);
    }

    public function dislike($id)
    {
        $user = Auth::user();
        $reacterFacade = $user->viaLoveReacter();

        $pertanyaan = Pertanyaan::find($id);
        $reacterFacade->reactTo($pertanyaan, 'Dislike');

        return redirect()->route('pertanyaan.show',["pertanyaan"=>$pertanyaan]);
    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form_post(Request $request){
        // dd($request->all());
        $first_name = $request['first_name'];
        $last_name = $request['last_name'];
        return view('welcoming', ['first_name' => $first_name, 'last_name' => $last_name]);
    }
}

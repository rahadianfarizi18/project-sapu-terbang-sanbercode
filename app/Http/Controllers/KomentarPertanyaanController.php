<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pertanyaan;
use App\komentarPertanyaan;
use Auth;
use Alert;

class KomentarPertanyaanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create($idPertanyaan)
    {
        $pertanyaan = Pertanyaan::find($idPertanyaan);
        return view('komentar_pertanyaan.create', compact('pertanyaan'));
    }


    public function store(Request $request)
    {

        date_default_timezone_set('Asia/Jakarta');
        $request->validate([
            'isi_komentar_pertanyaan' => 'required'
        ]);

        $user = Auth::user();

        $komentar_pertanyaan = $user->komentar_pertanyaan() -> create([
            'isi' => $request['isi_komentar_pertanyaan'],
            'tanggal_dibuat' => date('y/m/d'),
            'pertanyaan_id' => $request['id_pertanyaan']
        ]);

        $idPertanyaan = $komentar_pertanyaan -> pertanyaan_id;

        Alert::success('Berhasil', 'Komentar berhasil disimpan!');

        return redirect() -> route('pertanyaan.show',["pertanyaan"=>$idPertanyaan]);
    }

    public function edit($id)
    {
        $komentar_pertanyaan = komentarPertanyaan::with('user')->find($id);
        return view('komentar_pertanyaan.edit',compact('komentar_pertanyaan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        date_default_timezone_set('Asia/Jakarta');
        $request->validate([
            'isi_komentar_pertanyaan' => 'required'
        ]);

        $user = Auth::user();
        $komentar_pertanyaan = $user->komentar_pertanyaan() -> update([
            'isi' => $request['isi_komentar_pertanyaan']
        ]);

        $idPertanyaan = $request['id_pertanyaan'];
        Alert::success('Berhasil', 'Komentar berhasil diubah!');
        return redirect()->route('pertanyaan.show',["pertanyaan"=>$idPertanyaan]);
    }

    public function destroy($id, Request $request)
    {
        komentarPertanyaan::destroy($id);
        $idPertanyaan = $request['id_pertanyaan'];
        Alert::info('Berhasil', 'Komentar berhasil dihapus!');
        return redirect()->route('pertanyaan.show',["pertanyaan"=>$idPertanyaan]);
    }

    public function like($id, Request $request)
    {
        $user = Auth::user();
        $reacterFacade = $user->viaLoveReacter();

        $komentar_pertanyaan = komentarPertanyaan::find($id);
        $reacterFacade->reactTo($komentar_pertanyaan, 'Like');
        $pertanyaan = $komentar_pertanyaan -> pertanyaan -> id;

        return redirect()->route('pertanyaan.show',["pertanyaan"=>$pertanyaan]);
    }

    public function dislike($id, Request $request)
    {
        $user = Auth::user();
        $reacterFacade = $user->viaLoveReacter();

        $komentar_pertanyaan = komentarPertanyaan::find($id);
        $reacterFacade->reactTo($komentar_pertanyaan, 'Dislike');
        $pertanyaan = $komentar_pertanyaan -> pertanyaan -> id;

        return redirect()->route('pertanyaan.show',["pertanyaan"=>$pertanyaan]);
    }

}

<?php

namespace App;

use Cog\Contracts\Love\Reactable\Models\Reactable as ReactableInterface;
use Cog\Laravel\Love\Reactable\Models\Traits\Reactable;
use Illuminate\Database\Eloquent\Model;

class komentarJawaban extends Model implements ReactableInterface
{
    use Reactable;
    protected $table = 'komentar_jawaban';
    protected $guarded = [];

    public function user(){
        return $this->belongsTo('App\User','profil_id');
    }

    public function pertanyaan_tepat(){
        return $this->hasOne('App\Pertanyaan','jawaban_tepat_id');
    }

    public function jawaban(){
        return $this->belongsTo('App\Jawaban','jawaban_id');
    }
}

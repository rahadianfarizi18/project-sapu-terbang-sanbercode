<?php

namespace App;

use Cog\Contracts\Love\Reactable\Models\Reactable as ReactableInterface;
use Cog\Laravel\Love\Reactable\Models\Traits\Reactable;
use Illuminate\Database\Eloquent\Model;

class komentarPertanyaan extends Model implements ReactableInterface
{
    use Reactable;
    protected $table = 'komentar_pertanyaan';
    protected $guarded = [];

    public function user(){
        return $this->belongsTo('App\User','profil_id');
    }

    public function pertanyaan(){
        return $this->belongsTo('App\Pertanyaan','pertanyaan_id');
    }
}

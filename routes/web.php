<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PertanyaanController@index');

Route::get('/welcoming', function () {
    return view('welcoming');
});

Route::get('/register', function () {
    return view('register');
});

Route::post('/form_post', 'AuthController@form_post');

Route::get('/master', function(){
    return view('adminlte.master');
});

Route::get('/items',function(){
    return view('items.index');
});

Route::get('/data-tables',function(){
    return view('adminlte.partials.data-tables');
});

// Route::get('/pertanyaan','PertanyaanController@index');
// Route::get('/pertanyaan/create','PertanyaanController@create');
// Route::post('/pertanyaan','PertanyaanController@store');
// Route::get('/pertanyaan/{id}','PertanyaanController@show');
// Route::get('/pertanyaan/{id}/edit','PertanyaanController@edit');
// Route::put('/pertanyaan/{id}','PertanyaanController@update');
// Route::delete('/pertanyaan/{id}','PertanyaanController@destroy');

Route::resource('pertanyaan','PertanyaanController');
Route::get('/pertanyaan/{pertanyaan}/like','PertanyaanController@like')->name('pertanyaan.like');
Route::get('/pertanyaan/{pertanyaan}/dislike','PertanyaanController@dislike')->name('pertanyaan.dislike');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/test-dompdf', function(){
    $pdf = App::make('dompdf.wrapper');
    $pdf->loadHTML('<h1>Test</h1>');
    return $pdf->stream();
});

Route::get('export/', 'PertanyaanController@export');

Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});

Route::get('/demo-filemgr',function(){
    return view('file-manager.demo');
});

Route::post('/logout',"HomeController@logout");

Route::get('/jawaban/create/{jawaban}',['as' => "jawaban.create", 'uses' => "JawabanController@create"]);
Route::resource('jawaban','JawabanController', ['names' => [
    'create' => 'jawaban.create2'
    ]]);

Route::get('/jawaban/{jawaban}/like','JawabanController@like')->name('jawaban.like');
Route::get('/jawaban/{jawaban}/dislike','JawabanController@dislike')->name('jawaban.dislike');

Route::get('/jawaban/komentarjawaban/{pertanyaan}/{jawaban}',['as' => "komentar_jawaban.create", 'uses' => "komentarJawabanController@create"]);
Route::post('/jawaban/komentarjawaban','komentarJawabanController@store');
Route::delete('/jawaban/komentarjawaban/{komentarJawaban}','komentarJawabanController@destroy')->name('komentar_jawaban.destroy');

Route::get('/komentarjawaban/{komentar_jawaban}/like','komentarJawabanController@like')->name('komentar_jawaban.like');
Route::get('/komentarjawaban/{komentar_jawaban}/dislike','komentarJawabanController@dislike')->name('komentar_jawaban.dislike');

Route::get('komentarpertanyaan/{pertanyaan}/create','KomentarPertanyaanController@create') -> name('komentarpertanyaan.create');
Route::resource('komentarpertanyaan','KomentarPertanyaanController', ['names' => [
    'create' => 'komentar_pertanyaan.create2'
    ]]);


Route::get('/about',function(){
    return view('about.about');
});

Route::get('/komentarpertanyaan/{komentarpertanyaan}/like','KomentarPertanyaanController@like')->name('komentarpertanyaan.like');
Route::get('/komentarpertanyaan/{komentarpertanyaan}/dislike','KomentarPertanyaanController@dislike')->name('komentarpertanyaan.dislike');


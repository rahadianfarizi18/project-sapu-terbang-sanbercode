@extends('adminlte.master')
@push('script-head')
<script src="https://cdn.tiny.cloud/1/c708l487stbibqc62750iszmeyhq1oxoqcmparc625aejcod/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
@endpush

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Edit Pertanyaan {{ $pertanyaan->id }} </h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item active">General Form</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<form role="form" method="POST" action="/pertanyaan/{{ $pertanyaan->id }} ">
    @csrf
    @method('PUT')
    <div class="card-body">
        <div class="form-group">
            <label for="profilId">Profil ID</label>
            <input type="text" class="form-control" name= "profil_id" id="profilId" value="{{ $pertanyaan -> profil_id }}" disabled>
        </div>
        <div class="form-group">
            <label for="namaLengkap">Nama Lengkap</label>
            <input type="text" class="form-control" name= "namaLengkap" id="namaLengkap" value="{{ $pertanyaan -> user['name'] }}" disabled>
        </div>
        <div class="form-group">
            <label for="judulPertanyaan">Judul</label>
            <input type="text" class="form-control" name="judul_pertanyaan" id="judulPertanyaan" placeholder="Judul pertanyaan" value="{{old('judul_pertanyaan',$pertanyaan -> judul)}}">
            @error('judul_pertanyaan')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="tag">Tags</label>
            <input data-role = "tagsinput" type='text' class="form-control" name="tag" id="tag" placeholder="Tags..." value="{{ implode(',',$tags_arr) }}">
            @error('tag')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="isiPertanyaan">Pertanyaan</label>
            <textarea class="my-editor form-control" name="isi_pertanyaan" id="isiPertanyaan" rows = "10" placeholder="Pertanyaan anda...">{{old('isi_pertanyaan',$pertanyaan -> isi)}}</textarea>
            @error('isi_pertanyaan')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
    </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
</form>

@endsection

@push('script')
    <script>
    var editor_config = {
        path_absolute : "/",
        selector: "textarea.my-editor",
        plugins: [
        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime media nonbreaking save table contextmenu directionality",
        "emoticons template paste textcolor colorpicker textpattern"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
        relative_urls: false,
        file_browser_callback : function(field_name, url, type, win) {
        var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
        var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

        var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
        if (type == 'image') {
            cmsURL = cmsURL + "&type=Images";
        } else {
            cmsURL = cmsURL + "&type=Files";
        }

        tinyMCE.activeEditor.windowManager.open({
            file : cmsURL,
            title : 'Filemanager',
            width : x * 0.8,
            height : y * 0.8,
            resizable : "yes",
            close_previous : "no"
        });
        }
    };

    tinymce.init(editor_config);
    </script>
@endpush

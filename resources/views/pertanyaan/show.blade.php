@extends('adminlte.master')

@section('content')

    <div class="card card-widget">
        <div class="card-header">
            <div class="user-block">
                <img class="img-circle" src="{{asset('adminlte/dist/img/user1-128x128.jpg')}}" alt="User Image">
                <span class="username"><a href="#" class="text-reset"> {{$pertanyaan -> user['name']}} </a></span>
                <span class="description">Shared publicly on {{ $pertanyaan -> tanggal_dibuat }} </span>
            </div>
            <!-- /.user-block -->
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <h3> {{ $pertanyaan -> judul }} </h3>
            @foreach($pertanyaan->tag as $tag)
                <p class="text-muted" style='display:inline'>#{{ $tag->tag }} </p>
            @endforeach
            <p> {!! $pertanyaan -> isi !!} </p>
            <a href = " {{route('komentarpertanyaan.create', ['pertanyaan' => $pertanyaan->id] )}} " class="btn btn-default btn-sm"><i class="far fa-comment-dots"></i> Komentar</a>
            <a href="{{route('jawaban.create', ['jawaban' => $pertanyaan->id] )}}" class="btn btn-default btn-sm"><i class="fas fa-pen-alt"></i> Jawab</a>
            <button type="button" class="btn btn-default btn-sm"><i class="fas fa-share"></i> Share</button>
            @if(($reactantFacade->isReactedBy(Auth::user(), 'Like')) && ($reactantFacade->isNotReactedBy(Auth::user(), 'Dislike')))
                <a href=" {{ route('pertanyaan.like',['pertanyaan' => $pertanyaan->id]) }} " class="btn btn-default btn-sm disabled" aria-disabled="true"><i class="far fa-thumbs-up"></i> Like</a>
                <a href=" {{ route('pertanyaan.dislike',['pertanyaan' => $pertanyaan->id]) }} " class="btn btn-default btn-sm"><i class="far fa-thumbs-down"></i> Dislike</a>
            @elseif(($reactantFacade->isNotReactedBy(Auth::user(), 'Like')) && ($reactantFacade->isReactedBy(Auth::user(), 'Dislike')))
                <a href=" {{ route('pertanyaan.like',['pertanyaan' => $pertanyaan->id]) }} " class="btn btn-default btn-sm"><i class="far fa-thumbs-up"></i> Like</a>
                <a href=" {{ route('pertanyaan.dislike',['pertanyaan' => $pertanyaan->id]) }} " class="btn btn-default btn-sm disabled" aria-disabled="true"><i class="far fa-thumbs-down"></i> Dislike</a>
            @elseif(($reactantFacade->isReactedBy(Auth::user(), 'Like')) && ($reactantFacade->isReactedBy(Auth::user(), 'Dislike')))
                <a href=" {{ route('pertanyaan.like',['pertanyaan' => $pertanyaan->id]) }} " class="btn btn-default btn-sm disabled" aria-disabled="true"><i class="far fa-thumbs-up"></i> Like</a>
                <a href=" {{ route('pertanyaan.dislike',['pertanyaan' => $pertanyaan->id]) }} " class="btn btn-default btn-sm disabled" aria-disabled="true"><i class="far fa-thumbs-down"></i> Dislike</a>
            @else
                <a href=" {{ route('pertanyaan.like',['pertanyaan' => $pertanyaan->id]) }} " class="btn btn-default btn-sm"><i class="far fa-thumbs-up"></i> Like</a>
                <a href=" {{ route('pertanyaan.dislike',['pertanyaan' => $pertanyaan->id]) }} " class="btn btn-default btn-sm"><i class="far fa-thumbs-down"></i> Dislike</a>
            @endif
            <span class="float-right text-muted">
                {{ $reactantFacade->getReactionCounterOfType('Like')->getCount() }} likes
                {{ $reactantFacade->getReactionCounterOfType('Dislike')->getCount() }} dislikes
            </span>
        </div>
        @if(!$pertanyaan->komentar_pertanyaan->isEmpty())
            <div class="card-footer card-comments">
                @foreach($pertanyaan -> komentar_pertanyaan as $komentar_pertanyaan)
                    <div class="card-comment">
                        <img class="img-circle img-sm" src="{{asset('adminlte/dist/img/user3-128x128.jpg')}}" alt="User Image">
                        <div class="comment-text">
                            <span class="username"> {{ $komentar_pertanyaan->user['name'] }} <br/>
                                <span class="text-muted float-right"> {{ $komentar_pertanyaan->tanggal_dibuat }} </span>
                            </span><!-- /.username -->
                            {!! $komentar_pertanyaan->isi !!}
                        </div>
                            <!-- /.comment-text -->
                        @if(($komentar_pertanyaan -> viaLoveReactant()->isReactedBy(Auth::user(), 'Like')) && ($komentar_pertanyaan -> viaLoveReactant()->isNotReactedBy(Auth::user(), 'Dislike')))
                            <a href=" {{ route('komentarpertanyaan.like',['komentarpertanyaan' => $komentar_pertanyaan->id]) }} " class="btn btn-default btn-sm disabled" aria-disabled="true"><i class="far fa-thumbs-up"></i> Like</a>
                            <a href=" {{ route('komentarpertanyaan.dislike',['komentarpertanyaan' => $komentar_pertanyaan->id]) }} " class="btn btn-default btn-sm"><i class="far fa-thumbs-down"></i> Dislike</a>
                        @elseif(($komentar_pertanyaan -> viaLoveReactant()->isNotReactedBy(Auth::user(), 'Like')) && ($komentar_pertanyaan -> viaLoveReactant()->isReactedBy(Auth::user(), 'Dislike')))
                            <a href=" {{ route('komentarpertanyaan.like',['komentarpertanyaan' => $komentar_pertanyaan->id]) }} " class="btn btn-default btn-sm"><i class="far fa-thumbs-up"></i> Like</a>
                            <a href=" {{ route('komentarpertanyaan.dislike',['komentarpertanyaan' => $komentar_pertanyaan->id]) }} " class="btn btn-default btn-sm disabled" aria-disabled="true"><i class="far fa-thumbs-down"></i> Dislike</a>
                        @elseif(($komentar_pertanyaan -> viaLoveReactant()->isReactedBy(Auth::user(), 'Like')) && ($komentar_pertanyaan -> viaLoveReactant()->isReactedBy(Auth::user(), 'Dislike')))
                            <a href=" {{ route('komentarpertanyaan.like',['komentarpertanyaan' => $komentar_pertanyaan->id]) }} " class="btn btn-default btn-sm disabled" aria-disabled="true"><i class="far fa-thumbs-up"></i> Like</a>
                            <a href=" {{ route('komentarpertanyaan.dislike',['komentarpertanyaan' => $komentar_pertanyaan->id]) }} " class="btn btn-default btn-sm disabled" aria-disabled="true"><i class="far fa-thumbs-down"></i> Dislike</a>
                        @else
                            <a href=" {{ route('komentarpertanyaan.like',['komentarpertanyaan' => $komentar_pertanyaan->id]) }} " class="btn btn-default btn-sm"><i class="far fa-thumbs-up"></i> Like</a>
                            <a href=" {{ route('komentarpertanyaan.dislike',['komentarpertanyaan' => $komentar_pertanyaan->id]) }} " class="btn btn-default btn-sm"><i class="far fa-thumbs-down"></i> Dislike</a>
                        @endif
                        <span class="float-right text-muted">
                            {{ $komentar_pertanyaan -> viaLoveReactant()->getReactionCounterOfType('Like')->getCount() }} likes
                            {{ $komentar_pertanyaan -> viaLoveReactant()->getReactionCounterOfType('Dislike')->getCount() }} dislikes
                        </span>
                        @if(Auth::check() && (Auth::user()->id == $komentar_pertanyaan->user['id']))
                            <a href=" {{route('komentarpertanyaan.edit', ['komentarpertanyaan' => $komentar_pertanyaan->id] )}} "  type="button" class="btn btn-dark btn-sm ml-1 d-inline">edit</a>
                            <form class="d-inline" action=" {{ route('komentarpertanyaan.destroy', ['komentarpertanyaan' => $komentar_pertanyaan -> id]) }} " method="POST">
                                @csrf
                                @method('DELETE')
                                <input style="display:flex" type="submit" value="delete" class="btn btn-danger btn-sm ml-1 d-inline">
                                <input type="hidden" name= "id_pertanyaan" value=" {{ $pertanyaan->id }} ">
                            </form>
                        @endif
                    </div>
                    <!-- /.card-comment -->
                    <!-- /.card-footer -->
                @endforeach
            </div>
        @endif
    </div>
    @if(!$data_jawaban->isEmpty())
    <p class="h3 ml-3 font-weight-normal">Jawaban</p>
        @foreach($data_jawaban as $jawaban)
            <div class="card bg-secondary text-light card-widget mt-3">
                <div class="card-header">
                    <div class="user-block">
                        <img class="img-circle" src="{{asset('adminlte/dist/img/user1-128x128.jpg')}}" alt="User Image">
                        <span class="username"><a href="#" class="text-reset"> {{ $jawaban -> user ->name }} </a></span>
                        <span class="description text-light">Shared publicly on {{ $jawaban -> tanggal_dibuat }} </span>
                    </div>
                    <!-- /.user-block -->
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <h3> {{ $jawaban -> judul }} </h1>
                    <p> {!! $jawaban -> isi !!} </p>
                    <a href = " {{route('komentar_jawaban.create', ['jawaban' => $jawaban->id, 'pertanyaan' => $pertanyaan -> id] )}} " class="btn btn-default btn-sm"><i class="far fa-comment-dots"></i> Komentar</a>
                    @if(Auth::check() && (Auth::user()->id == $jawaban->user['id']))
                        <a href=" {{route('jawaban.edit', ['jawaban' => $jawaban->id] )}} "  type="button" class="btn btn-dark btn-sm ml-1 d-inline">edit</a>
                        <form class="d-inline" action=" {{ route('jawaban.destroy', ['jawaban' => $jawaban -> id]) }} " method="POST">
                            @csrf
                            @method('DELETE')
                            <input style="display:flex" type="submit" value="delete" class="btn btn-danger btn-sm ml-1 d-inline">
                            <input type="hidden" name= "id_pertanyaan" value=" {{ $pertanyaan->id }} ">
                        </form>
                    @endif
                    @if(($jawaban -> viaLoveReactant()->isReactedBy(Auth::user(), 'Like')) && ($jawaban -> viaLoveReactant()->isNotReactedBy(Auth::user(), 'Dislike')))
                        <a href=" {{ route('jawaban.like',['jawaban' => $jawaban->id]) }} " class="btn btn-default btn-sm disabled" aria-disabled="true"><i class="far fa-thumbs-up"></i> Like</a>
                        <a href=" {{ route('jawaban.dislike',['jawaban' => $jawaban->id]) }} " class="btn btn-default btn-sm"><i class="far fa-thumbs-down"></i> Dislike</a>
                    @elseif(($jawaban -> viaLoveReactant()->isNotReactedBy(Auth::user(), 'Like')) && ($jawaban -> viaLoveReactant()->isReactedBy(Auth::user(), 'Dislike')))
                        <a href=" {{ route('jawaban.like',['jawaban' => $jawaban->id]) }} " class="btn btn-default btn-sm"><i class="far fa-thumbs-up"></i> Like</a>
                        <a href=" {{ route('jawaban.dislike',['jawaban' => $jawaban->id]) }} " class="btn btn-default btn-sm disabled" aria-disabled="true"><i class="far fa-thumbs-down"></i> Dislike</a>
                    @elseif(($jawaban -> viaLoveReactant()->isReactedBy(Auth::user(), 'Like')) && ($jawaban -> viaLoveReactant()->isReactedBy(Auth::user(), 'Dislike')))
                        <a href=" {{ route('jawaban.like',['jawaban' => $jawaban->id]) }} " class="btn btn-default btn-sm disabled" aria-disabled="true"><i class="far fa-thumbs-up"></i> Like</a>
                        <a href=" {{ route('jawaban.dislike',['jawaban' => $jawaban->id]) }} " class="btn btn-default btn-sm disabled" aria-disabled="true"><i class="far fa-thumbs-down"></i> Dislike</a>
                    @else
                        <a href=" {{ route('jawaban.like',['jawaban' => $jawaban->id]) }} " class="btn btn-default btn-sm"><i class="far fa-thumbs-up"></i> Like</a>
                        <a href=" {{ route('jawaban.dislike',['jawaban' => $jawaban->id]) }} " class="btn btn-default btn-sm"><i class="far fa-thumbs-down"></i> Dislike</a>
                    @endif
                    <span class="float-right text-light">
                        {{ $jawaban -> viaLoveReactant()->getReactionCounterOfType('Like')->getCount() }} likes
                        {{ $jawaban -> viaLoveReactant()->getReactionCounterOfType('Dislike')->getCount() }} dislikes
                    </span>
                </div>
                <!-- /.card-body -->
                @if(!$data_komentar_jawaban->isEmpty())
                    <div class="card-footer card-comments">
                        @foreach($jawaban -> komentar_jawaban as $komentar_jawaban)
                            <div class="card-comment">
                                <img class="img-circle img-sm" src="{{asset('adminlte/dist/img/user3-128x128.jpg')}}" alt="User Image">
                                <div class="comment-text">
                                    <span class="username"> {{ $komentar_jawaban->user['name'] }} <br/>
                                        <span class="text-muted float-right"> {{ $komentar_jawaban->tanggal_dibuat }} </span>
                                    </span><!-- /.username -->
                                    {!! $komentar_jawaban->isi !!}
                                    @if (($komentar_jawaban -> viaLoveReactant()->isReactedBy(Auth::user(), 'Like')) && ($komentar_jawaban -> viaLoveReactant()->isNotReactedBy(Auth::user(), 'Dislike')))
                                        <a href=" {{ route('komentar_jawaban.like',['komentar_jawaban' => $komentar_jawaban->id]) }} " class="btn btn-default btn-sm disabled" aria-disabled="true"><i class="far fa-thumbs-up"></i> Like</a>
                                        <a href=" {{ route('komentar_jawaban.dislike',['komentar_jawaban'=> $komentar_jawaban->id]) }} " class="btn btn-default btn-sm"><i class="far fa-thumbs-down"></i> Dislike</a>
                                    @elseif (($komentar_jawaban -> viaLoveReactant()->isNotReactedBy(Auth::user(), 'Like')) & ($komentar_jawaban -> viaLoveReactant()->isReactedBy(Auth::user(), 'Dislike')))
                                        <a href=" {{ route('komentar_jawaban.like',['komentar_jawaban' => $komentar_jawaban->id]) }} " class="btn btn-default btn-sm"><i class="far fa-thumbs-up"></i> Like</a>
                                        <a href=" {{ route('komentar_jawaban.dislike',['komentar_jawaban' => $komentar_jawaban->id]) }} " class="btn btn-default btn-sm disabled" aria-disabled="true"><i class="far fa-thumbs-down"></i> Dislike</a>
                                    @elseif (($komentar_jawaban -> viaLoveReactant()->isReactedBy(Auth::user(), 'Like')) && ($komentar_jawaban -> viaLoveReactant()->isReactedBy(Auth::user(), 'Dislike')))
                                        <a href=" {{ route('komentar_jawaban.like',['komentar_jawaban' => $komentar_jawaban->id]) }} " class="btn btn-default btn-sm disabled" aria-disabled="true"><i class="far fa-thumbs-up"></i> Like</a>
                                        <a href=" {{ route('komentar_jawaban.dislike',['komentar_jawaban' => $komentar_jawaban->id]) }} " class="btn btn-default btn-sm disabled" aria-disabled="true"><i class="far fa-thumbs-down"></i> Dislike</a>
                                    @else
                                        <a href=" {{ route('komentar_jawaban.like',['komentar_jawaban' => $komentar_jawaban->id]) }} " class="btn btn-default btn-sm"><i class="far fa-thumbs-up"></i> Like</a>
                                        <a href=" {{ route('komentar_jawaban.dislike',['komentar_jawaban' => $komentar_jawaban->id]) }} " class="btn btn-default btn-sm"><i class="far fa-thumbs-down"></i> Dislike</a>
                                    @endif
                                    @if(Auth::check() && (Auth::user()->id == $komentar_jawaban->user['id']))
                                        <!-- <a href=" {{route('jawaban.edit', ['jawaban' => $jawaban->id] )}} "  type="button" class="btn btn-dark btn-sm ml-1 d-inline">edit</a> -->
                                        <form class="d-inline" action=" {{ route('komentar_jawaban.destroy', ['komentarJawaban' => $komentar_jawaban -> id]) }} " method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <input style="display:flex" type="submit" value="delete" class="btn btn-danger btn-sm ml-1 d-inline">
                                            <input type="hidden" name= "id_pertanyaan" value=" {{ $pertanyaan->id }} ">
                                        </form>
                                    @endif
                                    <span class="float-right text-muted">
                                        {{ $komentar_jawaban -> viaLoveReactant()->getReactionCounterOfType('Like')->getCount() }} likes
                                        {{ $komentar_jawaban -> viaLoveReactant()->getReactionCounterOfType('Dislike')->getCount() }} dislikes
                                    </span>
                                </div>
                                    <!-- /.comment-text -->
                            </div>
                        @endforeach
                    </div>
                @endif
                <!-- /.card-comment -->
                <!-- /.card-footer -->
            </div>
        @endforeach
    @endif
@endsection

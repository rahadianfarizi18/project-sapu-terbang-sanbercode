@extends('adminlte.master')

@section('content')

	<div class="container">
		<div class="d-flex justify-content-between pt-4">
			<div class="col-sm-4">
				<div class="card" style="width: 18rem;">
					<img src="{{asset('/images/gonewaje.jpg')}}" class="card-img-top" alt="image" style="width:286px;height:286px;">
					<div class="card-body">
					<h5 class="card-title">Profile</h5>
					<p class="card-text">
						<ul>
                    		<li>Name : Gonewaje</li>
                    		<li>Education : </li>
                    		<li>Current Job : </li>
               			</ul>
					</p>
					<a href="https://www.linkedin.com/in/gonewaje/" class="btn btn-primary">Linkedin</a>
					</div>
				</div>
			</div>

			<div class="col-sm-4">
				<div class="card" style="width: 18rem;">
					<img src="{{asset('/images/rahadianfarizi.JPG')}}" class="card-img-top" alt="image" style="width:286px;height:286px;">
					<div class="card-body">
					<h5 class="card-title">Profile</h5>
					<p class="card-text">
						<ul>
                    		<li>Name : Rahadian Farizi</li>
                    		<li>Education : </li>
                    		<li>Current Job : </li>
               			</ul>
					</p>
					<a href="https://www.linkedin.com/in/rahadian-farizi/" class="btn btn-primary">Linkedin</a>
					</div>
				</div>
			</div>

			<div class="col-sm-4">
				<div class="card" style="width: 18rem;">
					<img src="{{asset('adminlte/dist/img/index.png')}}" class="card-img-top" alt="image" style="width:286px;height:286px;">
					<div class="card-body">
					<h5 class="card-title">Profile</h5>
					<p class="card-text">
						<ul>
                    		<li>Name : Eveline Nelwan</li>
                    		<li>Education : </li>
                    		<li>Current Job : </li>
               			</ul>
					</p>
					<a href="https://www.linkedin.com/" class="btn btn-primary">Linkedin</a>
					</div>
				</div>
			</div>


		</div>
	</div>
@endsection

@extends('adminlte.master')

@push('script-head')
<script src="https://cdn.tiny.cloud/1/c708l487stbibqc62750iszmeyhq1oxoqcmparc625aejcod/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
@endpush

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-xl-6">
                <h1>Buat Komentar Baru</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item active">General Form</li>
                    <li class="breadcrumb-item active">Produk</li>
                    <li class="breadcrumb-item active">Category</li>
                    <li class="breadcrumb-item active">Shopping Cart</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<form role="form" method="POST" action="/jawaban/komentarjawaban">
    @csrf
    <div class="card-body">
        <h2>{{ $pertanyaan ->judul }}</h2>
        <p class="card-subtitle mt-2 text-muted font-italic">pertanyaan dari {{ $pertanyaan ->user['name'] }}</p>
        <p class="card-text">{!! $pertanyaan ->isi !!}</p>
        <input type="hidden" name="id_pertanyaan" value="{{ $pertanyaan ->id }}">
        <div class="card">
            <div class="card-body">
                <p class="card-subtitle text-muted font-italic">Jawaban dari {{ $jawaban ->user['name'] }}</p>
                <p class="card-text">{!! $jawaban ->isi !!}</p>
                <input type="hidden" name='id_jawaban' value="{{ $jawaban ->id }}">
            </div>
        </div>
        <div class="form-group">
            <label for="isi_jawaban">Komentar</label>
            <textarea id="isi_jawaban" name="isi_komentar_jawaban" class="my-editor form-control" rows="15">{{ old('isi_jawaban', '') }}</textarea>
            @error('isi_komentar_jawaban')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror

        </div>
    </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
</form>

@endsection

@push('script')
<script>
  var editor_config = {
    path_absolute : "/",
    selector: "textarea.my-editor",
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "emoticons template paste textcolor colorpicker textpattern"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
    relative_urls: false,
    file_browser_callback : function(field_name, url, type, win) {
      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

      var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
      if (type == 'image') {
        cmsURL = cmsURL + "&type=Images";
      } else {
        cmsURL = cmsURL + "&type=Files";
      }

      tinyMCE.activeEditor.windowManager.open({
        file : cmsURL,
        title : 'Filemanager',
        width : x * 0.8,
        height : y * 0.8,
        resizable : "yes",
        close_previous : "no"
      });
    }
  };

  tinymce.init(editor_config);
</script>

@endpush
